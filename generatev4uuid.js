function generateV4Uuid () {
  // apparently IE11 has msCrypto
  var _rns = (window.crypto || window.msCrypto).getRandomValues(new Uint8Array(16));
  var _uuid = [];
  var _i = 0;

  /* 
    See:
    - https://datatracker.ietf.org/doc/html/rfc4122#section-4.4
    - https://github.com/broofa/node-uuid/blob/5233610462ccf42ee06aaf12c72d9c54e88d81b8/v4.js#L16
  */
  _rns[6] = (_rns[6] & 0x0f) | 0x40;
  _rns[8] = (_rns[8] & 0x3f) | 0x80;

  // map rns to hexadecimal
  for (; _i < _rns.length; ++_i) {
    _uuid[_i] = ((_rns[_i] + 0x100).toString(16).substr(1));
  }
  // join with hyphens where required
  return _uuid.reduce(function (x, y, z) {
    switch (z) {
      case 4:
      case 6:
      case 8:
      case 10:
        return x + '-' + y;
      default:
        return x + y;
    }
  }).toLowerCase();
}
