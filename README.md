# generateV4Uuid.js
### Generate V4 type UUIDs in the browser

Just ... does what it says on the tin really.

There's no shortage of JS implementations for this task, but I just wanted a simple, single-file single-function that I can deploy when needed.
